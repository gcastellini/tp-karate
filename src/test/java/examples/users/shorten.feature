@prueba
Feature: Shorten API

  Background: URL y Headers
    Given url urlBase
    And path pathShorten


  Scenario: Consulta exitosa
    * def body = read('classpath:examples/users/request/url.json')
    Given header x-rapidapi-key = apiKey
    And request body
    When method POST
    Then status 200
    And match response.result_url == '#string'


  Scenario: Consulta sin body
    * def body = read('classpath:examples/users/request/urlEmpty.json')
    Given header x-rapidapi-key = apiKey
    And request body
    When method POST
    Then status 400
    And match response.error == "API Error: URL is empty"


  Scenario: Consulta URL inválida
    * def body = read('classpath:examples/users/request/urlInvalid.json')
    Given header x-rapidapi-key = apiKey
    And request body
    When method POST
    Then status 400
    And match response.error == "API Error: URL is invalid (check #1)"


  Scenario: Consulta Token inválido
    * def body = read('classpath:examples/users/request/url.json')
    Given header x-rapidapi-key = 'asasaa'
    And request body
    When method POST
    Then status 403
    And match response.message == "You are not subscribed to this API."